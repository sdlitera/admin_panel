import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getClientsAll} from '../store/clients/actions';
import Loader from '../views/Loader';
import ClientList from '../views/ClientList/ClientsList';


const ClientsListContainer = () => {
  const dispatch = useDispatch();
  const {authToken} = useSelector(state => state.layout.loginData);
  const {clientsData} = useSelector(state => state.clients);
  const getAll = (page) => {
    const pageUrl = page ? `?page=${page}` : ''
    return dispatch(getClientsAll(pageUrl, authToken));
  };
  useEffect(() => {
    getAll()
  }, [])
  if (clientsData && clientsData.data) {
    return (
        <ClientList getClientsAll={getAll} clients={clientsData}/>
    );
  }
  return (<Loader />)

};

export default ClientsListContainer;