import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getOrdersAll} from '../store/orders/actions';
import Loader from '../views/Loader';
import OrderList from '../views/OrderList/OrderList';

const OrderListContainer = () => {
  const dispatch = useDispatch();
  const {authToken} = useSelector(state => state.layout.loginData);
  const {ordersData} = useSelector(state => state.orders);
  const getAll = (page) => {
    const pageUrl = page ? `?page=${page}` : ''
    return dispatch(getOrdersAll(pageUrl, authToken));
  };
  useEffect(() => {
    getAll()
  }, [])
  if (ordersData && ordersData.data) {
    return (
        <OrderList getOrdersAll={getAll} orders={ordersData}/>
    );
  }
  return (<Loader />)

};

export default OrderListContainer;