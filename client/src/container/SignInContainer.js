import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {login} from '../store/layout/actions';
import SignInForm from '../views/SignIn/SignInForm';

const SignInContainer = () => {
  const dispatch = useDispatch();
  const {pathname} = useSelector(state => state.router.location);
  const loginF = (username, password) => {
    return dispatch(login(username, password, pathname));
  };
  return (
      <SignInForm login={loginF}/>
  );
};

export default SignInContainer;