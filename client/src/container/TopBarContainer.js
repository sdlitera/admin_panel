import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {logout} from '../store/layout/actions';
import TopBarComponent from '../layouts/Main/components/Topbar/Topbar';

const MainTopBarContainer = (props) => {
  const dispatch = useDispatch();
  const {authToken} = useSelector(state => state.layout.loginData);
  const logoutF = () => {
    return dispatch(logout(authToken));
  };
  return (
      <TopBarComponent logout={logoutF} {...props}/>
  );
};

export default MainTopBarContainer;