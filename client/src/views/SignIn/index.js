import React from 'react';
import SignInContainer from '../../container/SignInContainer';

const SignIn = (props) => {
  return (
      <SignInContainer {...props}/>
  );
};

export default SignIn;
