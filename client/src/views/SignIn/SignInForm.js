import React, {useState, useEffect} from 'react';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import {makeStyles} from '@material-ui/core/styles';
import {
  Grid,
  Button,
  TextField,
  Typography,
} from '@material-ui/core';

const schema = {
  login: {
    presence: {allowEmpty: false, message: 'is required'},
    length: {
      maximum: 64,
    },
  },
  password: {
    presence: {allowEmpty: false, message: 'is required'},
    length: {
      maximum: 128,
    },
  },
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.default,
    height: '100%',
  },
  grid: {
    height: '100%',
  },
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    },
  },
  form: {
    paddingLeft: 100,
    paddingRight: 100,
    paddingBottom: 125,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    },
  },
  title: {
    marginTop: theme.spacing(3),
  },
  textField: {
    marginTop: theme.spacing(2),
  },
  signInButton: {
    margin: theme.spacing(2, 0),
  },
}));

const SignInForm = props => {
  const {login} = props;

  const classes = useStyles();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {},
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: !errors,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
            event.target.type === 'checkbox'
                ? event.target.checked
                : event.target.value,
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true,
      },
    }));
  };

  const handleSignIn = event => {
    event.preventDefault();
    login(formState.values.login, formState.values.password);
  };

  const hasError = field => !!(formState.touched[field] && formState.errors[field]);

  return (
      <div className={classes.root}>
        <Grid
            className={classes.grid}
            container
        >
          <Grid
              className={classes.content}
              item
              lg={3}
              xs={false}
          />
          <Grid
              className={classes.content}
              item
              lg={6}
              xs={12}
          >
            <div className={classes.content}>
              <div className={classes.contentBody}>
                <form
                    className={classes.form}
                    onSubmit={handleSignIn}
                >
                  <Typography
                      className={classes.title}
                      variant="h2"
                  >
                    Sign in
                  </Typography>
                  <TextField
                      className={classes.textField}
                      error={hasError('login')}
                      fullWidth
                      helperText={
                        hasError('login') ? formState.errors.login[0] : null
                      }
                      label="Login"
                      name="login"
                      onChange={handleChange}
                      type="text"
                      value={formState.values.login || ''}
                      variant="outlined"
                  />
                  <TextField
                      className={classes.textField}
                      error={hasError('password')}
                      fullWidth
                      helperText={
                        hasError('password') ?
                            formState.errors.password[0] :
                            null
                      }
                      label="Password"
                      name="password"
                      onChange={handleChange}
                      type="password"
                      value={formState.values.password || ''}
                      variant="outlined"
                  />
                  <Button
                      className={classes.signInButton}
                      color="primary"
                      disabled={!formState.isValid}
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                  >
                    Sign in now
                  </Button>
                </form>
              </div>
            </div>
          </Grid>
          <Grid
              className={classes.content}
              item
              lg={3}
              xs={false}
          />
        </Grid>
      </div>
  );
};

SignInForm.propTypes = {
  history: PropTypes.object,
};

export default withRouter(SignInForm);
