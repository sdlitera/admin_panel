import React from 'react';
import {makeStyles} from '@material-ui/core/styles';

import OrdersToolbar from './components/OrdersToolbar';
import OrdersTable from './components/OrdersTable';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const OrderList = (props) => {
  const classes = useStyles();
  return (
      <div className={classes.root}>
        {/*<OrdersToolbar/>*/}
        <div className={classes.content}>
          <OrdersTable {...props}/>
        </div>
      </div>
  );
};

export default OrderList;
