import React, {useState} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {makeStyles} from '@material-ui/core/styles';
import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  TablePagination,
} from '@material-ui/core';

import {getInitials} from '../../../../helpers';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0,
  },
  inner: {
    minWidth: 1050,
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    marginRight: theme.spacing(2),
  },
  actions: {
    justifyContent: 'flex-end',
  },
}));

const OrdersTable = props => {
  const {className, orders, getOrdersAll, ...rest} = props;
  const classes = useStyles();

  const ordersArray = orders.data;

  const page = orders.current_page - 1;
  const rowsPerPage = orders.per_page;
  const handlePageChange = (event, page) => {
    getOrdersAll(page + 1)
  };
  return (
      <Card
          {...rest}
          className={clsx(classes.root, className)}
      >
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell>UserId</TableCell>
                    <TableCell>Quantity</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>OrderId</TableCell>
                    <TableCell>OrderData</TableCell>
                    <TableCell>ProductCode</TableCell>
                    <TableCell>Info</TableCell>
                    <TableCell>pay_pal_status</TableCell>
                    <TableCell>client ID</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {ordersArray.slice(0, rowsPerPage).map(client => (
                      <TableRow
                          className={classes.tableRow}
                          hover
                          key={client.ID}
                      >
                        <TableCell>{client.ID}</TableCell>
                        <TableCell>{client.UserId}</TableCell>
                        <TableCell>{client.Quantity}</TableCell>
                        <TableCell>{client.Price}</TableCell>
                        <TableCell>{client.Status}</TableCell>
                        <TableCell>{client.OrderId}</TableCell>
                        <TableCell>{client.OrderData}</TableCell>
                        <TableCell>{client.ProductCode}</TableCell>
                        <TableCell>{client.Info}</TableCell>
                        <TableCell>{client.pay_pal_status && client.pay_pal_status.code}</TableCell>
                        <TableCell>{client.client && client.client.ID}</TableCell>
                      </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
              component="div"
              count={orders.total}
              onChangePage={handlePageChange}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[15]}
          />
        </CardActions>
      </Card>
  );
};

OrdersTable.propTypes = {
  className: PropTypes.string,
  orders: PropTypes.object.isRequired,
  getOrdersAll: PropTypes.func.isRequired,
};

export default OrdersTable;
