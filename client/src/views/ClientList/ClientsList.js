import React from 'react';
import {makeStyles} from '@material-ui/core/styles';

import ClientsTable from './components/ClientsTable/ClientsTable';
import ClientsToolbar from './components/ClientsToolbar/ClientsToolbar';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const ClientList = (props) => {
  const classes = useStyles();
  return (
      <div className={classes.root}>
        {/*<ClientsToolbar/>*/}
        <div className={classes.content}>
          <ClientsTable {...props}/>
        </div>
      </div>
  );
};

export default ClientList;
