import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {makeStyles} from '@material-ui/core/styles';
import {
  Card,
  CardActions,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TablePagination,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0,
  },
  inner: {
    minWidth: 1050,
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    marginRight: theme.spacing(2),
  },
  actions: {
    justifyContent: 'flex-end',
  },
}));

const ClientsTable = props => {
  const {className, clients, getClientsAll, ...rest} = props;
  const classes = useStyles();

  const clientsArray = clients.data;

  const page = clients.current_page - 1;
  const rowsPerPage = clients.per_page;
  const handlePageChange = (event, page) => {
    getClientsAll(page + 1)
  };
  return (
      <Card
          {...rest}
          className={clsx(classes.root, className)}
      >
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>RegisterData</TableCell>
                    <TableCell>Adress</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Tell</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {clientsArray.slice(0, rowsPerPage).map(client => (
                      <TableRow
                          className={classes.tableRow}
                          hover
                          key={client.ID}
                      >
                        <TableCell>{client.ID}</TableCell>
                        <TableCell>{client.Name}</TableCell>
                        <TableCell>{client.RegistrData}</TableCell>
                        <TableCell>{client.Adress}</TableCell>
                        <TableCell>{client.Email}</TableCell>
                        <TableCell>{client.Tell}</TableCell>
                      </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
              component="div"
              count={clients.total}
              onChangePage={handlePageChange}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[15]}
          />
        </CardActions>
      </Card>
  );
};

ClientsTable.propTypes = {
  className: PropTypes.string,
  clients: PropTypes.object.isRequired,
  getClientsAll: PropTypes.func.isRequired,
};

export default ClientsTable;
