import React from 'react';
import MainTopBarContainer from '../../../../container/TopBarContainer';

const TopBar = (props) => {
  return (
      <MainTopBarContainer {...props}/>
  );
};

export default TopBar;