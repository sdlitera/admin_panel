import React, {useState} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import {
  AppBar,
  Toolbar,
  Badge,
  Hidden,
  IconButton,
  Typography,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import {ROUTE_CLIENTS} from '../../../../constants/routes';

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: 'none',
  },
  flexGrow: {
    flexGrow: 1,
  },
  signOutButton: {
    marginLeft: theme.spacing(1),
  },
  link: {
    color: 'white',
  },
}));

const TopBarComponent = props => {
  const {className, onSidebarOpen, logout, ...rest} = props;

  const classes = useStyles();

  const [notifications] = useState([]);

  const logoutOnClick = () => {
    logout()
  }

  return (
      <AppBar
          {...rest}
          className={clsx(classes.root, className)}
      >
        <Toolbar>
          <RouterLink to={ROUTE_CLIENTS}>
            <Typography
                className={classes.link}
                variant="button"
            >
              Litera admin panel
            </Typography>
          </RouterLink>
          <div className={classes.flexGrow}/>
          <Hidden mdDown>
            <IconButton color="inherit">
              <Badge
                  badgeContent={notifications.length}
                  color="primary"
                  variant="dot"
              >
                <NotificationsIcon/>
              </Badge>
            </IconButton>
            <IconButton onClick={logoutOnClick}
                className={classes.signOutButton}
                color="inherit"
            >
              <InputIcon/>
            </IconButton>
          </Hidden>
          <Hidden lgUp>
            <IconButton
                color="inherit"
                onClick={onSidebarOpen}
            >
              <MenuIcon/>
            </IconButton>
          </Hidden>
        </Toolbar>
      </AppBar>
  );
};

TopBarComponent.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func,
  logout: PropTypes.func,
};

export default TopBarComponent;
