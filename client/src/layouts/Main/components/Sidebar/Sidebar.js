import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import {Drawer} from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import AssignmentIcon from '@material-ui/icons/Assignment';

import {SidebarNav} from './components';
import {
  ROUTE_CLIENTS,
  ROUTE_ORDERS,
  ROUTE_PRODUCTS,
} from '../../../../constants/routes';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)',
    },
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2),
  },
  divider: {
    margin: theme.spacing(2, 0),
  },
  nav: {
    marginBottom: theme.spacing(2),
  },
}));

const Sidebar = props => {
  const {open, variant, onClose, className, ...rest} = props;

  const classes = useStyles();

  const pages = [
    {
      title: 'Clients',
      href: ROUTE_CLIENTS,
      icon: <PeopleIcon/>,
    },
    {
      title: 'Products',
      href: ROUTE_PRODUCTS,
      icon: <ShoppingBasketIcon/>,
    },
    {
      title: 'Orders',
      href: ROUTE_ORDERS,
      icon: <AssignmentIcon/>,
    },
  ];

  return (
      <Drawer
          anchor="left"
          classes={{paper: classes.drawer}}
          onClose={onClose}
          open={open}
          variant={variant}
      >
        <div
            {...rest}
            className={clsx(classes.root, className)}
        >
          <SidebarNav
              className={classes.nav}
              pages={pages}
          />
        </div>
      </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired,
};

export default Sidebar;
