export const API_URL = 'https://dem.berezka.ml';

export const LOGIN_URL = 'auth/login';
export const GET_MY_INFO_URL = 'auth/me'; //
export const REFRESH_URL = 'auth/refresh'; // refresh token
export const LOGOUT_URL = 'auth/logout';

export const CLIENTS_URL = 'clients'
export const ORDERS_URL = 'orders'
export const PRODUCTS_URL = 'products'