import React, {Component} from 'react';
import {Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import history from './store/history';
import {ThemeProvider} from '@material-ui/core/styles';
import validate from 'validate.js';
import store from './store/store';

import theme from './theme';
import 'react-perfect-scrollbar/dist/css/styles.css';
import './assets/scss/index.scss';
import validators from './common/validators';
import Routes from './Routes';

validate.validators = {
  ...validate.validators,
  ...validators,
};

export default class App extends Component {
  render() {
    return (
        <Provider store={store}>
          <ThemeProvider theme={theme}>
            <Router history={history}>
              <Routes/>
            </Router>
          </ThemeProvider>
        </Provider>
    );
  }
}
