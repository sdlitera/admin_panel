import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import layout from './layout/reducer';
import clients from './clients/reducer';
import orders from './orders/reducer';
import products from './products/reducer';

const rootReducer = (history) => combineReducers({
  router: connectRouter(history),
  layout,
  clients,
  orders,
  products
});

export default rootReducer;