import {callApi} from '../../helpers/api';
import {
  API_URL,
  LOGIN_URL, LOGOUT_URL,
} from '../../constants/api';
import history from '../history';
import {LOCAL_STORAGE_U} from '../../constants';
import {ROUTE_CLIENTS, ROUTE_ROOT} from '../../constants/routes';

export const LOGIN_REQUESTED = '@@layout/LOGIN_REQUEST';
export const LOGIN = '@@layout/LOGIN';
export const LOGOUT_REQUESTED = '@@layout/LOGOUT_REQUESTED';
export const LOGOUT = '@@layout/LOGOUT';

export const login = (username, password, path) => {
  return dispatch => {
    dispatch({
      type: LOGIN_REQUESTED,
    });
    return callApi('POST', API_URL, LOGIN_URL, {
      login: username,
      password: password,
    }).then(data => {
      if (data && data.token) {
        data = {
          ...data,
          authToken: `Bearer ${data.token}`
        }
        localStorage.setItem(LOCAL_STORAGE_U, JSON.stringify(data))
        dispatch({
          type: LOGIN,
          payload: data,
        });
        history.push(ROUTE_CLIENTS)
      }
    }).catch(err => {
      console.error(err);
    });

  };
};

export const updateLoginData = (ls) => {
  return dispatch => {
    dispatch({
      type: LOGIN,
      payload: ls,
    });
    history.push(ROUTE_CLIENTS)
  }
}

export const logout = (token) => {
  return dispatch => {
    dispatch({
      type: LOGOUT_REQUESTED,
    });
    return callApi('POST', API_URL, LOGOUT_URL, {}, token).then(data => {
        localStorage.removeItem(LOCAL_STORAGE_U)
        dispatch({
          type: LOGOUT,
        });
        history.push(ROUTE_ROOT)
    }).catch(err => {
      console.error(err);
    });

  };
};