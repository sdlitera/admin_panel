import {LOGIN, LOGIN_REQUESTED, LOGOUT, LOGOUT_REQUESTED} from './actions';

export const initialState = {
  loginData: {},
  isLoggedIn: false,
  isLoggining: false,
  isLogouting: false,
};

export default function layout(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUESTED:
      return {
        ...state,
        isLoggining: true,
      };

    case LOGIN:
      return {
        ...state,
        loginData: action.payload,
        isLoggedIn: !state.isLoggedIn,
        isLoggining: false,
      };

    case LOGOUT_REQUESTED:
      return {
        ...state,
        isLogouting: true,
      };

    case LOGOUT:
      return {
        ...state,
        loginData: {},
        isLoggedIn: false,
        isLogouting: false,
      };

    default:
      return state;
  }
}