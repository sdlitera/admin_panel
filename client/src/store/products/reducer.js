import {GET_PRODUCTS, GET_PRODUCTS_REQUESTED} from './actions';

export const initialState = {
  productsData: [],
  isLoading: false
};

export default function products(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCTS_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };

    case GET_PRODUCTS:
      return {
        ...state,
        productsData: action.payload,
        isLoading: !state.isLoading,
      };

    default:
      return state;
  }
}