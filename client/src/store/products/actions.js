import {callApi} from '../../helpers/api';
import {
  API_URL, PRODUCTS_URL,
} from '../../constants/api';
import history from '../history';
import {ROUTE_PRODUCTS} from '../../constants/routes';

export const GET_PRODUCTS_REQUESTED = '@@products/GET_PRODUCTS_REQUESTED';
export const GET_PRODUCTS = '@@products/GET_PRODUCTS';

export const getProductsAll = (page, token) => {
  return dispatch => {
    dispatch({
      type: GET_PRODUCTS_REQUESTED,
    });
    return callApi('GET', API_URL, `${PRODUCTS_URL}${page}`,null, token).then(data => {
      if (data) {
        dispatch({
          type: GET_PRODUCTS,
          payload: data,
        });
        history.push(`${ROUTE_PRODUCTS}${page}`)
      }
    }).catch(err => {
      console.error(err);
    });
  };
};