import {callApi} from '../../helpers/api';
import {
  API_URL, ORDERS_URL,
} from '../../constants/api';

export const GET_ORDERS_REQUESTED = '@@orders/GET_ORDERS_REQUESTED';
export const GET_ORDERS = '@@orders/GET_ORDERS';

export const getOrdersAll = (page, token) => {
  return dispatch => {
    dispatch({
      type: GET_ORDERS_REQUESTED,
    });
    return callApi('GET', API_URL, `${ORDERS_URL}${page}`,null, token).then(data => {
      if (data) {
        dispatch({
          type: GET_ORDERS,
          payload: data,
        });
      }
    }).catch(err => {
      console.error(err);
    });
  };
};