import {GET_ORDERS, GET_ORDERS_REQUESTED} from './actions';

export const initialState = {
  ordersData: [],
  isLoading: false
};

export default function orders(state = initialState, action) {
  switch (action.type) {
    case GET_ORDERS_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };

    case GET_ORDERS:
      return {
        ...state,
        ordersData: action.payload,
        isLoading: !state.isLoading,
      };

    default:
      return state;
  }
}