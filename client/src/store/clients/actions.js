import {callApi} from '../../helpers/api';
import {
  API_URL, CLIENTS_URL,
} from '../../constants/api';

export const GET_CLIENTS_REQUESTED = '@@clients/GET_CLIENTS_REQUESTED';
export const GET_CLIENTS = '@@clients/GET_CLIENTS';

export const getClientsAll = (page, token) => {
  return dispatch => {
    dispatch({
      type: GET_CLIENTS_REQUESTED,
    });
    return callApi('GET', API_URL, `${CLIENTS_URL}${page}`,null, token).then(data => {
      if (data) {
        dispatch({
          type: GET_CLIENTS,
          payload: data,
        });
      }
    }).catch(err => {
      console.error(err);
    });
  };
};