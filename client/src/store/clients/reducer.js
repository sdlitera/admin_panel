import {GET_CLIENTS, GET_CLIENTS_REQUESTED} from './actions';

export const initialState = {
  clientsData: {},
  isLoading: false
};

export default function clients(state = initialState, action) {
  switch (action.type) {
    case GET_CLIENTS_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };

    case GET_CLIENTS:
      return {
        ...state,
        clientsData: action.payload,
        isLoading: !state.isLoading,
      };

    default:
      return state;
  }
}