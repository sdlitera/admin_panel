import {createStore, compose, applyMiddleware} from 'redux';
import {connectRouter, routerMiddleware} from 'connected-react-router';
import history from './history';
import rootReducer from './combineReducer';
import thunk from 'redux-thunk';

const initialState = {};
const enhancers = [];

const middleware = [
  thunk,
  routerMiddleware(history),
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers,
);

const store = createStore(
    connectRouter(history)(rootReducer(history)),
    initialState,
    composedEnhancers,
);

export default store;