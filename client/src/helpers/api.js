export async function callApi(method, url, path, data, token) {
  let body = {
    method,
    headers: {
      "Authorization": token,
      "Content-Type": "application/json"
    },
    // body: JSON.stringify(data),
  }
  if (method === 'POST') {
    body = Object.assign({},body, {body: JSON.stringify(data)})
  }
  const res = await fetch(`${url}/${path}`, body).catch(err => {
    console.error('api err', err)
  });
  return res.json();
}