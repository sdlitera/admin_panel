import React, {useEffect} from 'react';
import {Switch, Redirect} from 'react-router-dom';

import {RouteWithLayout} from './components';
import {Main as MainLayout, Minimal as MinimalLayout} from './layouts';

import {
  ProductList as ProductListView,
  ClientList as ClientListView,
  OrderList as OrderListView,
  SignIn as SignInView,
  NotFound as NotFoundView,
} from './views';
import {useDispatch, useSelector} from 'react-redux';
import {LOCAL_STORAGE_U} from './constants';
import {updateLoginData} from './store/layout/actions';
import {
  ROUTE_CLIENTS,
  ROUTE_CLIENTS_PER_PAGE,
  ROUTE_NOT_FOUND,
  ROUTE_ORDERS,
  ROUTE_PRODUCTS,
  ROUTE_ROOT,
  ROUTE_SIGN_IN,
} from './constants/routes';

const Routes = () => {
  const {isLoggedIn} = useSelector(state => state.layout);
  const dispatch = useDispatch();
  const updateLogin = (ls) => {
    return dispatch(updateLoginData(ls));
  };
  const ls = localStorage.getItem(LOCAL_STORAGE_U)
  useEffect(() => {
    if (!isLoggedIn && ls) {
      updateLogin(JSON.parse(ls))
    }
  }, [isLoggedIn])
  return (
      <Switch>
        <Redirect
            exact
            from={ROUTE_ROOT}
            to={ROUTE_SIGN_IN}
        />
        {isLoggedIn && <RouteWithLayout
            component={ClientListView}
            exact
            layout={MainLayout}
            path={ROUTE_CLIENTS}
        />}
        {isLoggedIn && <RouteWithLayout
            component={ClientListView}
            layout={MainLayout}
            path={ROUTE_CLIENTS_PER_PAGE}
        />}
        {isLoggedIn &&<RouteWithLayout
            component={ProductListView}
            exact
            layout={MainLayout}
            path={ROUTE_PRODUCTS}
        />}
        {isLoggedIn &&<RouteWithLayout
            component={OrderListView}
            exact
            layout={MainLayout}
            path={ROUTE_ORDERS}
        />}
        <RouteWithLayout
            component={SignInView}
            exact
            layout={MinimalLayout}
            path={ROUTE_SIGN_IN}
        />
        <RouteWithLayout
            component={NotFoundView}
            exact
            layout={MinimalLayout}
            path={ROUTE_NOT_FOUND}
        />
        <Redirect to={ROUTE_NOT_FOUND}/>
      </Switch>
  );
};

export default Routes;
